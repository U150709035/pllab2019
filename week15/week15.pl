use strict;

use warnings;


my $filename = $ARGV[0];
open IN, '<', $filename or die "Could not open file $filename!\n";


my @lines = <IN>;

close IN;

foreach my $line (@lines) {
    chomp $line;
    my $regex = '\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b';
    if($line =~ /$regex/) { print "$1.$2.$3.$4\n"; }



}






=pod

while (my $row = <IN>) {
    chomp $row;
    print "$row\n";
}



chomp $input;



my $regex = '^\s*$';
$regex = '^[A-Z]+$';
$regex = '[A-Z]\d*';
$regex = '^\d+\.+\d+$';
$regex = '^(\d{1,3}\.){3}\d{1,3}$';
$regex = '\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b';

until(($input eq "quit")||($input eq "q")) {

	if($input =~ /$regex/) { print "ACCEPTED\n"; }

	else { print "FAILED\n";}

	print "type q or quit to exit\n";

	print "Enter your input:";

	$input = <>;

	chomp $input;

}

=cut
