SELECT *  FROM Customers WHERE CustomerID = 20;

DELETE FROM Customers WHERE CustomerID = 20;


SELECT * FROM Orders WHERE CustomerID = 20;

DELETE FROM Orders WHERE CustomerID = 20;


SELECT * FROM OrderDetails;

SELECT * FROM OrderDetails WHERE OrderID in (SELECT OrderID FROM Orders WHERE CustomerID = 20);

DELETE FROM OrderDetails WHERE OrderID in (SELECT OrderID FROM Orders WHERE CustomerID = 20);

####################################################################

SELECT * FROM Products;

SELECT * FROM Products WHERE ProductID = 10;

DELETE FROM Products WHERE ProductID = 10;


SELECT * FROM OrderDetails WHERE ProductID = 10;

DELETE FROM OrderDetails WHERE ProductID = 10;

####################################################################

SELECT * FROM Shippers;

DELETE FROM Shippers WHERE ShipperID = 2;


SELECT OrderID FROM Orders WHERE ShipperID = 2;

DELETE FROM Orders WHERE ShipperID = 2;


SELECT * FROM OrderDetails WHERE OrderID in (SELECT OrderID FROM Orders WHERE ShipperID = 2);

DELETE FROM OrderDetails WHERE OrderID in (SELECT OrderID FROM Orders WHERE ShipperID = 2);
